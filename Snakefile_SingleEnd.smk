# AQUAMIS - Assembly-based QUAlity assessment for Microbial Isolate Sequencing

# NOTE. This is a copy from the Snakefile with adapations for a single end assembly workflow

import pandas as pd
import yaml
from confindr_src import confindr

# %% Settings -------------------------------------------------------

shell.executable("bash")

# Set snakemake main workdir variable
workdir: config["workdir"]

# Import rule directives
with open(config["smk_params"]["rule_directives"],"r") as stream:
    try:
        rule_directives = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)
        rule_directives = {'rule': ''}

# Collect sample names from sample list
samples = pd.read_csv(config["samples"],index_col="sample",sep="\t")
samples.index = samples.index.astype('str',copy=False)  # in case samples are integers, need to convert them to str

# %% Functions ------------------------------------------------------

def _get_fastq(wildcards, read_pair='fq1'):
    return samples.loc[wildcards.sample, [read_pair]].dropna()[0]

# %% All and Local Rules  -------------------------------------------

localrules: all, all_trimming_only, all_ephemeral
localrules: software_versions, database_versions, init_fails_file, delete_ephemeral_sample, delete_ephemeral_dirs

rule all:
    input:
        # Pre-Assembly Results
        "reports/software_versions.tsv",  # software_versions
        "reports/database_versions.md5",  # database_versions
        expand("trimmed/reports/{sample}.json",sample=samples.index),  # run_fastp
        expand("confindr/{sample}/confindr_report.csv",sample=samples.index),  # run_confindr
        expand("kraken2/{sample}.species.bracken",sample=samples.index),  # run_braken_reads_species
        expand("kraken2/{sample}.genus.bracken",sample=samples.index),  # run_braken_reads_genus
        expand("json/pre_assembly/{sample}.aquamis.json",sample=samples.index),  # parse_json_before_assembly
        expand("Assembly/assembly/{sample}.fasta",sample=samples.index),  # assembly_cleanup
        "reports/fastp_report.html",  # write_pre_assembly_report

        # Post-Assembly Results
        expand("Assembly/{sample}/vcf/samstats.txt",sample=samples.index),  # run_samtools
        expand("Assembly/{sample}/mash/mash_best.dist",sample=samples.index),  # run_mash
        expand("Assembly/{sample}/quast/report.tsv",sample=samples.index),  # run_quast
        expand("Assembly/{sample}/quast/genome_stats/genome_info.txt",sample=samples.index),  # run_quast
        expand("Assembly/{sample}/mlst/report.json",sample=samples.index),  # run_mlst
        expand("json/post_assembly/{sample}.aquamis.json",sample=samples.index),  # parse_json_after_assembly
        "reports/assembly_report.html",  # write_report
        expand("json/post_qc/{sample}.aquamis.json",sample=samples.index),  # parse_json_after_qc
        expand("json/filtered/{sample}.aquamis.json",sample=samples.index),  # filter_json

rule all_trimming_only:
    input:
        "reports/software_versions.tsv",
        "reports/database_versions.md5",
        expand("trimmed/reports/{sample}.json",sample=samples.index),
        expand("confindr/{sample}/confindr_report.csv",sample=samples.index),
        expand("kraken2/{sample}.species.bracken",sample=samples.index),
        expand("kraken2/{sample}.genus.bracken",sample=samples.index),
        expand("json/pre_assembly/{sample}.aquamis.json",sample=samples.index),
        "reports/fastp_report.html"

rule all_ephemeral:
    input:
        # Pre-Assembly Results
        "reports/software_versions.tsv",
        "reports/database_versions.md5",
        expand("json/pre_assembly/{sample}.aquamis.json",sample=samples.index),
        "reports/fastp_report.html",  # Comment-out to run on HighTroughput Mode

        # Post-Assembly Results
        expand("json/post_assembly/{sample}.aquamis.json",sample=samples.index),
        expand("logs/{sample}_ephemeral_data.log",sample=samples.index),
        expand("json/post_qc/{sample}.aquamis.json", sample=samples.index),  # Comment-out to run on HighTroughput Mode
        expand("json/filtered/{sample}.aquamis.json",sample=samples.index),  # Comment-out to run on HighTroughput Mode
        "reports/assembly_report.html",  # Comment-out to run on HighTroughput Mode

        # Receipts for Deletion of Ephemeral Data
        "reports/data_availability.log"

# %% software and database version rules ----------------------------

rule software_versions:
    output:
        temp=temp("reports/shovill_versions.tsv"),
        versions="reports/software_versions.tsv"
    group: rule_directives.get("software_versions", {}).get("group", None)
    priority: rule_directives.get("software_versions", {}).get("priority", None)
    resources:
        cpus=1,
        time=3
    message:
        "Parsing software versions"
    log:
        "logs/software_versions.log"
    shell:
        """
        shovill --check 2> {output.temp}
        echo -e "Software\tVersion" > {output.versions}
        fastp --version 2>> {output.versions}
        confindr --version >> {output.versions}
        kraken2 --version | head -n 1 | sed 's/ version//' >> {output.versions}
        grep VERSION $(which bracken | head -n 1) | sed -E 's/VERSION="(.*)"/bracken \\1/' >> {output.versions}
        taxonkit version >> {output.versions}
        shovill --version >> {output.versions}
        perl -pe 's/\[shovill\] Using (.+) - .*? \| .*?(\d+\.\d+\.*\d*-*\w*).*/\\1\t\\2/' {output.temp} >> {output.versions}
        echo -e "mash\t$(mash --version)" >> {output.versions}
        quast --version | grep "QUAST" >> {output.versions}
        mlst --version >> {output.versions}
        blastn -version | grep -P "^blastn" | tr -d ':+' >> {output.versions}
        sed -E 's/ [v]*/\t/' -i {output.versions}
        """

rule database_versions:
    input:
        db_kraken=config["params"]["kraken2"]["db_kraken"] + '/hash.k2d',
        db_mash=config["params"]["mash"]["mash_refdb"],
        json_thresholds=config["params"]["qc"]["thresholds"],
        json_schema=config["params"]["json_schema"]["validation"],
        json_filter=config["params"]["json_schema"]["filter"]
    output:
        db_hashes="reports/database_versions.md5"
    group: rule_directives.get("database_versions", {}).get("group", None)
    priority: rule_directives.get("database_versions", {}).get("priority", None)
    resources:
        cpus=1,
        time=4
    message:
        "Parsing database versions"
    log:
        "logs/database_versions.log"
    run:
        import hashlib
        def checksum_md5(filename, buffer_size=(128 * (2 ** 9))):  # read data in 64k chunks
            md5sum = hashlib.md5()
            with open(filename,'rb') as f:
                for chunk in iter(lambda: f.read(buffer_size),b''):
                    md5sum.update(chunk)
            return md5sum.hexdigest()
        hashes = {input.db_kraken      : checksum_md5(filename=input.db_kraken),
                  input.db_mash        : checksum_md5(filename=input.db_mash),
                  input.json_thresholds: checksum_md5(filename=input.json_thresholds),
                  input.json_schema    : checksum_md5(filename=input.json_schema),
                  input.json_filter    : checksum_md5(filename=input.json_filter)}
        with open(output.db_hashes,'w') as output_file:
            {output_file.write("{}\t{}\n".format(value,key)) for (key, value) in hashes.items()}
        print("Databases hashed with MD5.")

rule init_fails_file:
    output:
        "Assembly/fails.txt"
    group: rule_directives.get("init_fails_file", {}).get("group", None)
    priority: rule_directives.get("init_fails_file", {}).get("priority", None)
    message:
        "Creating empty file fails.txt"
    shell:
        "touch {output}"

# %% trimming rule --------------------------------------------------

rule run_fastp:
    input:
        r1=lambda wildcards: _get_fastq(wildcards,'fq1')
    params:
        length_required=config["params"]["fastp"]["length_required"],
        n_base_limit=50  # if one read's number of N base is >n_base_limit, then this read/pair is discarded
    output:
        r1="trimmed/{sample}_R1.fastq.gz",
        json="trimmed/reports/{sample}.json",
        html="trimmed/reports/{sample}.html"
    group: rule_directives.get("run_fastp", {}).get("group", None)
    priority: rule_directives.get("run_fastp", {}).get("priority", None)
    resources:
        cpus=config["smk_params"]["threads"],
        mem_mb=4000,
        time=2
    message:
        "Running Fastp on sample {wildcards.sample}"
    log:
        "logs/{sample}_fastp.log"
    threads:
        config["smk_params"]["threads"]
    shell:
        """
        fastp --verbose --thread {threads} --in1 {input.r1} --out1 {output.r1} --n_base_limit {params.n_base_limit} --json {output.json} --html {output.html} --report_title 'Sample {wildcards.sample}' --length_required {params.length_required} &> {log}
        """

# %% confindr rule --------------------------------------------------

rule run_confindr:
    input:
        r1="trimmed/{sample}_R1.fastq.gz"
    params:
        confindr_dir="confindr/{sample}",
        databases_folder=config["params"]["confindr"]["database"],
        cross_details=True,
        keep_files=False,
        min_matching_hashes=40,
        use_rmlst=False
    output:
        confindr="confindr/{sample}/confindr_report.csv"
    group: rule_directives.get("run_confindr", {}).get("group", None)
    priority: rule_directives.get("run_confindr", {}).get("priority", None)
    resources:
        cpus=config["smk_params"]["threads"],
        mem_mb=4000,
        time=4
    message:
        "Running Confindr on sample {wildcards.sample}"
    log:
        "logs/{sample}_confindr.log"
    threads:
        config["smk_params"]["threads"]
    run:
        import fileinput
        print("Confindr analysis of sample " + wildcards.sample)
        try:
            confindr.find_contamination(pair=[input.r1],
                                        forward_id='_R1',
                                        threads=threads,
                                        output_folder=params.confindr_dir,
                                        keep_files=params.keep_files,
                                        quality_cutoff=20,
                                        base_cutoff=2,
                                        base_fraction_cutoff=0.05,
                                        data_type='Illumina',
                                        use_rmlst=params.use_rmlst,
                                        cross_details=params.cross_details,
                                        min_matching_hashes=params.min_matching_hashes,
                                        databases_folder=params.databases_folder)
        except:
            print("Confindr analysis failed! Writing a dummy result.")
            with open(output.confindr, 'w', newline='') as outfile:
                outfile.write('Sample,Genus,NumContamSNVs,ContamStatus,PercentContam,PercentContamStandardDeviation,BasesExamined,DatabaseDownloadDate\n')
                outfile.write(wildcards.sample + ',ND,ND,ND,ND,ND,ND,ND\n')
        for line in fileinput.input(output.confindr, inplace=True):
            print(line.replace("_R1,", ","), end="")

# %% kraken2_reads rules --------------------------------------------

rule run_kraken2_reads:
    input:
        r1="trimmed/{sample}_R1.fastq.gz"
    params:
        db_kraken=config["params"]["kraken2"]["db_kraken"]
    output:
        kraken_report="kraken2/{sample}.fromreads.kreport",
        kraken_out=temp("kraken2/{sample}.fromreads.kraken")
    group: rule_directives.get("run_kraken2_reads", {}).get("group", None)
    priority: rule_directives.get("run_kraken2_reads", {}).get("priority", None)
    resources:
        cpus=config["smk_params"]["threads"],
        mem_mb=16000,
        time=4
    message:
        "Running Kraken2 on reads of sample {wildcards.sample}"
    log:
        "logs/{sample}_kraken2_reads.log"
    threads:
        config["smk_params"]["threads"]
    shell:
        """
        kraken2 --threads {threads} --gzip-compressed --db {params.db_kraken} --output {output.kraken_out} --report {output.kraken_report} {input.r1}
        """

rule run_braken_reads_species:
    input:
        "kraken2/{sample}.fromreads.kreport"
    params:
        db_kraken=config["params"]["kraken2"]["db_kraken"],
        read_length=config["params"]["kraken2"]["read_length"],
        taxonomic_level="S",
        threshold=0
    output:
        bracken_report="kraken2/{sample}.species.breport",
        bracken_out="kraken2/{sample}.species.bracken"
    group: rule_directives.get("run_braken_reads_species", {}).get("group", None)
    priority: rule_directives.get("run_braken_reads_species", {}).get("priority", None)
    resources:
        cpus=1
    message:
        "Running Bracken-species on reads of sample {wildcards.sample}"
    log:
        "logs/{sample}_bracken_species.log"
    shell:
        """
        bracken -d {params.db_kraken} -i {input} -o {output.bracken_out} -w {output.bracken_report} -r {params.read_length} -l {params.taxonomic_level} -t {params.threshold} | tee {log} 2> {log}
        """

rule run_braken_reads_genus:
    input:
        "kraken2/{sample}.fromreads.kreport"
    params:
        db_kraken=config["params"]["kraken2"]["db_kraken"],
        read_length=config["params"]["kraken2"]["read_length"],
        taxonomic_level="G",
        threshold=0
    output:
        bracken_report="kraken2/{sample}.genus.breport",
        bracken_out="kraken2/{sample}.genus.bracken"
    group: rule_directives.get("run_braken_reads_genus", {}).get("group", None)
    priority: rule_directives.get("run_braken_reads_genus", {}).get("priority", None)
    resources:
        cpus=1
    message:
        "Running Bracken-genus on reads of sample {wildcards.sample}"
    log:
        "logs/{sample}_bracken_genus.log"
    shell:
        """
        bracken -d {params.db_kraken} -i {input} -o {output.bracken_out} -w {output.bracken_report} -r {params.read_length} -l {params.taxonomic_level} -t {params.threshold} | tee {log} 2> {log}
        """

# %% pre-assembly json rule -----------------------------------------

rule parse_json_before_assembly:
    input:
        software_versions="reports/software_versions.tsv",
        db_hashes="reports/database_versions.md5",
        fastp="trimmed/reports/{sample}.json",
        confindr_report="confindr/{sample}/confindr_report.csv",
        kraken_reads_report="kraken2/{sample}.fromreads.kreport",
        bracken_species_report="kraken2/{sample}.species.bracken",
        bracken_genus_report="kraken2/{sample}.genus.bracken"
    params:
        sample="{sample}",
        config=config
    output:
        json_out="json/pre_assembly/{sample}.aquamis.json"
    group: rule_directives.get("parse_json_before_assembly", {}).get("group", None)
    priority: rule_directives.get("parse_json_before_assembly", {}).get("priority", None)
    resources:
        cpus=1
    message:
        "Aggregating PreAssembly results in JSON format of sample {wildcards.sample}"
    log:
        log="logs/{sample}_parseJson_beforeAssembly.log"
    script:
        "scripts/parse_json.py"

# %% pre-assembly report rule ---------------------------------------

rule write_pre_assembly_report:
    input:
        json_in=expand("json/pre_assembly/{sample}.aquamis.json",sample=samples.index)
    params:
        csv_fastp="reports/summary_fastp.tsv",
        csv_confindr="reports/summary_confindr.csv",
        csv_bracken="reports/summary_bracken.tsv"
    output:
        "reports/fastp_report.html"  # TODO: ,str(float(data["adapter_cutting"]["adapter_trimmed_bases"])/float(data["summary"]["before_filtering"]["total_bases"])*100)]
    group: rule_directives.get("write_pre_assembly_report", {}).get("group", None)
    priority: rule_directives.get("write_pre_assembly_report", {}).get("priority", None)
    resources:
        cpus=1,
        mem_mb=2000,
        time=2
    message:
        "Creating QC Rmarkdown report"
    log:
        log="logs/write_QC_report.log"
    script:
        "scripts/write_QC_report.Rmd"  # TODO: Compare collect_bracken with bracken_table, they are not the same!

# %% assembly rules -------------------------------------------------

rule run_shovill:
    input:
        r1="trimmed/{sample}_R1.fastq.gz"
    params:
        path2bin=workflow.basedir + '/scripts/shovill_SingleEnd/shovill',
        depth=config["params"]["shovill"]["depth"],
        outdir=directory("Assembly/{sample}"),
        output_options=config["params"]["shovill"]["output_options"],
        tmpdir=config["params"]["shovill"]["tmpdir"],
        ram=config["params"]["shovill"]["ram"],
        assembler=config["params"]["shovill"]["assembler"],
        kmers='--kmers "{}"'.format(config["params"]["shovill"]["kmers"]) if config["params"]["shovill"]["kmers"] else "",
        extraopts='--opts "{}"'.format(config["params"]["shovill"]["extraopts"]) if config["params"]["shovill"]["extraopts"] else "",
        modules=config["params"]["shovill"]["modules"]
    output:
        contigs="Assembly/{sample}/contigs.fa",
        alignment=temp("Assembly/{sample}/shovill.bam"),
        logfile=("Assembly/{sample}/shovill.log")
    group: rule_directives.get("run_shovill", {}).get("group", None)
    priority: rule_directives.get("run_shovill", {}).get("priority", None)
    resources:
        cpus=config["smk_params"]["threads"] * config["params"]["shovill"]["thread_factor"],
        mem_mb=config["params"]["shovill"]["ram"] * 1000,
        time=30
    message:
        "Running Assembly on sample {wildcards.sample}"
    log:
        "logs/{sample}_assembly.log"
    threads:
        config["smk_params"]["threads"] * config["params"]["shovill"]["thread_factor"]
    shell:
        """
        {params.path2bin} {params.extraopts} --keepfiles --force {params.modules} {params.output_options} {params.kmers} --assembler {params.assembler} --depth {params.depth} --tmpdir {params.tmpdir} --cpus {threads} --ram {params.ram} --outdir {params.outdir} --se {input.r1} &> {log}
        """

rule assembly_cleanup:
    input:
        contigs="Assembly/{sample}/contigs.fa"
    params:
        remove_temp=config["smk_params"]["remove_temp"]
    output:
        contigs="Assembly/assembly/{sample}.fasta"
    group: rule_directives.get("assembly_cleanup", {}).get("group", None)
    priority: rule_directives.get("assembly_cleanup", {}).get("priority", None)
    resources:
        cpus=1
    message:
        "Post-Shovill housekeeping of sample {wildcards.sample}"
    log:
        "logs/{sample}_cleanup.log"
    shell:
        """
        [ -f Assembly/{wildcards.sample}/spades.log ] && mv --verbose Assembly/{wildcards.sample}/spades.log logs/{wildcards.sample}_spades.log > {log}
        [ {params.remove_temp} == "True" ] && rm --verbose -rf Assembly/{wildcards.sample}/spades/ >> {log}
        [ {params.remove_temp} == "True" ] && rm --verbose -f Assembly/{wildcards.sample}/R1.sub.fq.gz >> {log}
        [ {params.remove_temp} == "True" ] && rm --verbose -f Assembly/{wildcards.sample}/R2.sub.fq.gz >> {log}
        cp --verbose {input.contigs} {output.contigs} >> {log}
        """

# %% kraken2_contigs rules ------------------------------------------

rule run_kraken2_contigs:
    input:
        contigs="Assembly/{sample}/contigs.fa"
    params:
        kraken2_db=config["params"]["kraken2"]["db_kraken"],
        kraken_report="kraken2/{sample}.fromcontigs.kreport"
    output:
        kraken_out="kraken2/{sample}.fromcontigs.kraken"
    group: rule_directives.get("run_kraken2_contigs", {}).get("group", None)
    priority: rule_directives.get("run_kraken2_contigs", {}).get("priority", None)
    resources:
        cpus=config["smk_params"]["threads"],
        mem_mb=16000,
        time=2
    message:
        "Running Kraken2 on contigs of sample {wildcards.sample}"
    log:
        "logs/{sample}_kraken2_contigs.log"
    threads:
        config["smk_params"]["threads"]
    shell:
        """
        kraken2 --db {params.kraken2_db} --threads {threads} --output {output} --report {params.kraken_report} {input}
        """

rule run_taxonkits_contigs:
    input:
        kraken_out="kraken2/{sample}.fromcontigs.kraken"
    params:
        taxondb=config["params"]["kraken2"]["taxonkit_db"]
    output:
        taxonkit_tmp=temp("kraken2/{sample}.fromcontigs.taxonkit.tmp"),
        taxonkit_out="kraken2/{sample}.fromcontigs.taxonkit"
    group: rule_directives.get("run_taxonkits_contigs", {}).get("group", None)
    priority: rule_directives.get("run_taxonkits_contigs", {}).get("priority", None)
    resources:
        cpus=1,
        mem_mb=4000
    message:
        "Running TaxonKit on contigs of sample {wildcards.sample}"
    shell:
        """
        cut -f 3 {input.kraken_out} | taxonkit lineage --data-dir {params.taxondb} | taxonkit reformat --data-dir {params.taxondb} --format "k__{{k}}|p__{{p}}|c__{{c}}|o__{{o}}|f__{{f}}|g__{{g}}|s__{{s}}" --miss-rank-repl "unclassified" | cut -f 3 > {output.taxonkit_tmp}
        awk 'BEGIN{{FS="\t"}}; FNR==NR{{ a[FNR""] = $2 FS $3 FS $4; next }}{{ print a[FNR""] FS $0 }}' {input.kraken_out} {output.taxonkit_tmp} > {output.taxonkit_out}
        """

# %% mash rules for best reference identification -------------------

rule run_mash:
    input:
        "Assembly/{sample}/contigs.fa"
    params:
        sketch="Assembly/{sample}/mash/mash",# only needed if two commands
        mashDB=config["params"]["mash"]["mash_refdb"],
        kmersize=config["params"]["mash"]["mash_kmersize"],
        sketchsize=config["params"]["mash"]["mash_sketchsize"]
    output:
        distance=temp("Assembly/{sample}/mash/mash.dist"),
        topmash="Assembly/{sample}/mash/mash_best.dist"
    group: rule_directives.get("run_mash", {}).get("group", None)
    priority: rule_directives.get("run_mash", {}).get("priority", None)
    resources:
        cpus=1
    message:
        "Running Mash on sample {wildcards.sample}"
    log:
        "logs/{sample}_mash.log"
    shell:
        """
        mash sketch -p {threads} -k {params.kmersize} -s {params.sketchsize} -o {params.sketch} {input} | tee {log} 2> {log}
        mash dist {params.mashDB} {params.sketch}.msh > {output.distance}  | tee -a {log} 2> {log}
        awk 'BEGIN {{OFS="\t"}}; {{split($5,a,"/"); print $1, $2, $3, $4, a[1], a[2]}}' {output.distance} | sort -nr -k 5 > {output.topmash}
        """

rule download_mash_reference:
    input:
        distance="Assembly/{sample}/mash/mash_best.dist"
    params:
        mashDB=config["params"]["mash"]["mash_refdb"],
        docker=config["smk_params"]["docker"],
        protocol=config["params"]["mash"]["mash_protocol"]  # NOTE: use "true" if host allows ftp connections to the NCBI, it is much faster for gff files
    output:
        ref_fasta="Assembly/{sample}/ref/reference.fasta",
        ref_gff="Assembly/{sample}/ref/reference.gff"
    group: rule_directives.get("download_mash_reference", {}).get("group", None)
    priority: rule_directives.get("download_mash_reference", {}).get("priority", None)
    resources:
        cpus=1
    message:
        "Downloading Mash reference on sample {wildcards.sample}"
    log:
        "logs/{sample}_mash_download.log"
    shell:
        """
        ## init
        if [ -z "{params.docker}" ]
        then
            reference_genome_db="$(dirname {params.mashDB})/genomes"
        else
            reference_genome_db="$(dirname {input.distance})/genomes"
        fi
        echo "Using reference repository: $reference_genome_db" > {log}
        reference_dict=${{reference_genome_db}}/accession_dict.tsv
        [ ! -d "${{reference_genome_db}}" ] && mkdir -p "$reference_genome_db"
        [ ! -f "$reference_dict" ] && touch "$reference_dict"
        
        ## retrieve accession id
        accession=$(awk 'NR==1{{print $1}}' {input.distance} | grep -oP '[A-Z]{{2,3}}_[A-Z0-9]+[.]*[0-9]*')
        echo "Best accession is: $accession" >> {log}
        
        ## query reference repository via dictionary
        if grep -q "^$accession" "$reference_dict"
        then  # copy
            fna_ftp_file=$(awk -v accession=$accession 'BEGIN{{FS=OFS="\t"}} {{if ($1 == accession) print $2;}}' "$reference_dict")
            gff_ftp_file=${{fna_ftp_file/.fna.gz/.gff.gz}}
            echo "Copying accession FASTA & GFF files from reference repository: $fna_ftp_file" >> {log}
        else  # download
            echo "The fasta file is not present in the reference repository. Proceeding with download from NCBI." >> {log}
            echo -e "$accession\tawaiting_ncbi_retrieval" >> "$reference_dict"  # this adds an entry to log incomplete online queries
            
            ## retrieval of ftp path
            fna_ftp_path=$(esearch -db assembly -query "${{accession}}" | esummary | xtract -pattern DocumentSummary -element FtpPath_RefSeq | awk 'BEGIN{{FS="/"}} END{{print $0"/"$NF"_genomic.fna.gz"}}')
            fna_ftp_file=$(basename $fna_ftp_path)
            gff_ftp_file=${{fna_ftp_file/.fna.gz/.gff.gz}}  # presence of gff file at NCBI is a prerequisite
            gff_ftp_path=$(dirname $fna_ftp_path)/$gff_ftp_file
            
            ## download with wget to reference repository
            if [[ "{params.protocol}" == "https" ]]
            then  # IMPORTANT: these "nuccore" files are single-fasta/gff3 and may only contain a single chromosome of multi-chromosome species 
                echo "Best accession is $accession" > {log}
                echo "wget -O - https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?db=nuccore\&report=fasta\&id=$accession --tries 10 | gzip > ${{reference_genome_db}}/$fna_ftp_file"  >> {log}
                      wget -O - https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?db=nuccore\&report=fasta\&id=$accession --tries 10 | gzip > ${{reference_genome_db}}/$fna_ftp_file
                echo "wget -O - https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?db=nuccore\&report=gff3\&id=$accession  --tries 10 | gzip > ${{reference_genome_db}}/$gff_ftp_file" >> {log}
                      wget -O - https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?db=nuccore\&report=gff3\&id=$accession  --tries 10 | gzip > ${{reference_genome_db}}/$gff_ftp_file
            else  # IMPORTANT: these "assembly|refseq" files may include multiple chromosomes and associated plasmids in a multi-fasta/gff3 format 
                echo "wget -O ${{reference_genome_db}}/$fna_ftp_file $fna_ftp_path --tries 10" >> {log}
                      wget -O ${{reference_genome_db}}/$fna_ftp_file $fna_ftp_path --tries 10
                echo "wget -O ${{reference_genome_db}}/$gff_ftp_file $gff_ftp_path --tries 10" >> {log}
                      wget -O ${{reference_genome_db}}/$gff_ftp_file $gff_ftp_path --tries 10
            fi
            awk -v accession=$accession -v ftpfile=$fna_ftp_file 'BEGIN{{FS=OFS="\t"}} {{if ($1 == accession) $2=ftpfile;}} 1' "$reference_dict" > tmp.tsv && mv -f tmp.tsv "$reference_dict"
        fi
        
        ## decompress from reference repository
        gzip --decompress --to-stdout ${{reference_genome_db}}/${{fna_ftp_file}} > {output.ref_fasta}
        gzip --decompress --to-stdout ${{reference_genome_db}}/${{gff_ftp_file}} > {output.ref_gff}
        """

# %% qc rules -------------------------------------------------------

rule run_samtools:
    input:
        alignment="Assembly/{sample}/shovill.bam"
    output:
        stats="Assembly/{sample}/vcf/samstats.txt",
        view_count="Assembly/{sample}/vcf/samstats_view_count.txt"
    group: rule_directives.get("run_samtools", {}).get("group", None)
    priority: rule_directives.get("run_samtools", {}).get("priority", None)
    resources:
        cpus=1,
        time=2
    message:
        "Collecting read mapping statistics of sample {wildcards.sample}"
    log:
        "logs/{sample}_samtools.log"
    shell:
        """
        samtools stats --coverage 1,10000,1 {input.alignment} > {output.stats}
        samtools view {input.alignment} | cut -f 10 | wc -m > {output.view_count}
        """

rule run_quast:
    input:
        contigs="Assembly/{sample}/contigs.fa",
        ref_gff="Assembly/{sample}/ref/reference.gff",
        ref_fasta="Assembly/{sample}/ref/reference.fasta"
    params:
        outdir="Assembly/{sample}/quast/",
        min_contig=config["params"]["quast"]["min_contig"],
        min_identity=config["params"]["quast"]["min_identity"]
    output:
        "Assembly/{sample}/quast/report.tsv",
        "Assembly/{sample}/quast/genome_stats/genome_info.txt",
        "Assembly/{sample}/quast/transposed_report.tsv",
        "Assembly/{sample}/quast/contigs_reports/unaligned_report.tsv",
        "Assembly/{sample}/quast/contigs_reports/transposed_report_misassemblies.tsv"
        # "Assembly/{sample}/quast/busco_stats/short_summary_{sample}.txt"  # BUSCO
    group: rule_directives.get("run_quast", {}).get("group", None)
    priority: rule_directives.get("run_quast", {}).get("priority", None)
    resources:
        cpus=config["smk_params"]["threads"],
        mem_mb=4000,
        time=8
    message:
        "Running Quast on sample {wildcards.sample}"
    log:
        "logs/{sample}_quast.log"
    threads:
        config["smk_params"]["threads"]
    shell:
        """
        quast --threads {threads} --conserved-genes-finding --output-dir {params.outdir} -r {input.ref_fasta} --features {input.ref_gff} -L {input.contigs} --min-contig {params.min_contig} --min-identity {params.min_identity} | tee {log} 2> {log}
        """

# %% mlst -----------------------------------------------------------

rule run_mlst:
    input:
        contigs="Assembly/{sample}/contigs.fa"
    params:
        scheme=('--scheme ' + config["params"]["mlst"]["scheme"]) if config["params"]["mlst"]["scheme"] else '',
        label="{sample}",
        tmpreport="Assembly/{sample}/mlst/report.tsv.tmp"
    output:
        report="Assembly/{sample}/mlst/report.tsv",
        json="Assembly/{sample}/mlst/report.json"
    group: rule_directives.get("run_mlst", {}).get("group", None)
    priority: rule_directives.get("run_mlst", {}).get("priority", None)
    resources:
        cpus=1,
        time=8
    message:
        "Running MLST on sample {wildcards.sample}"
    log:
        "logs/{sample}_mlst.log"
    shell:
        """
        mlst --json {output.json} --label {params.label} {params.scheme} {input.contigs} > {params.tmpreport} 2> >(tee {log} >&2)
        echo -e "{wildcards.sample}\t$(cut -f 2- {params.tmpreport})" > {output.report}
        """

# %% post-assembly json rules ---------------------------------------

rule parse_json_after_assembly:
    input:
        json_in="json/pre_assembly/{sample}.aquamis.json",
        software_versions="reports/software_versions.tsv",
        db_hashes="reports/database_versions.md5",
        assembly="Assembly/{sample}/contigs.fa",
        assembly_log="Assembly/{sample}/shovill.log",
        samstats="Assembly/{sample}/vcf/samstats.txt",
        samstats_view_count="Assembly/{sample}/vcf/samstats_view_count.txt",
        kraken_contigs_report="kraken2/{sample}.fromcontigs.taxonkit",
        mash_distance="Assembly/{sample}/mash/mash_best.dist",
        reference="Assembly/{sample}/ref/reference.fasta",
        quast_report="Assembly/{sample}/quast/report.tsv",
        quast_genome_info="Assembly/{sample}/quast/genome_stats/genome_info.txt",
        quast_misassemblies="Assembly/{sample}/quast/contigs_reports/transposed_report_misassemblies.tsv",
        quast_unaligned="Assembly/{sample}/quast/contigs_reports/unaligned_report.tsv",
        mlst="Assembly/{sample}/mlst/report.json"
    params:
        sample="{sample}",
        config=config,
        busco="Assembly/{sample}/quast/busco_stats/short_summary_{sample}.txt"
    output:
        json_out="json/post_assembly/{sample}.aquamis.json"
    group: rule_directives.get("parse_json_after_assembly", {}).get("group", None)
    priority: rule_directives.get("parse_json_after_assembly", {}).get("priority", None)
    resources:
        cpus=1
    message:
        "Aggregating PostAssembly results in JSON format of sample {wildcards.sample}"
    log:
        log="logs/{sample}_parseJson_afterAssembly.log"
    script:
        "scripts/parse_json.py"

# %% post-assembly report rule --------------------------------------

rule write_report:
    input:
        json_in=expand("json/post_assembly/{sample}.aquamis.json", sample=samples.index),
        fails="Assembly/fails.txt"
    params:
        tsv_kraken2_contigs="reports/summary_kraken2_contigs.tsv",
        tsv_assembly="reports/summary_assembly.tsv",
        tsv_report="reports/summary_report.tsv",
        tsv_qc_vote="reports/summary_qc.tsv",
        software_versions="reports/software_versions.tsv"
    output:
        "reports/assembly_report.html"
    group: rule_directives.get("write_report", {}).get("group", None)
    priority: rule_directives.get("write_report", {}).get("priority", None)
    resources:
        cpus=1,
        mem_mb=2000,
        time=2
    message:
        "Creating Rmarkdown report"
    log:
        log="logs/write_report.log"
    script:
        "scripts/write_report.Rmd"

# %% post-qc report rules -------------------------------------------

rule parse_json_after_qc:
    input:
        json_in="json/post_assembly/{sample}.aquamis.json",
        software_versions="reports/software_versions.tsv",
        db_hashes="reports/database_versions.md5",
        required="reports/assembly_report.html"
    params:
        sample="{sample}",
        config=config,
        tsv_qc_vote="reports/summary_qc.tsv"
    output:
        json_out="json/post_qc/{sample}.aquamis.json"
    group: rule_directives.get("parse_json_after_qc", {}).get("group", None)
    priority: rule_directives.get("parse_json_after_qc", {}).get("priority", None)
    resources:
        cpus=1
    message:
        "Appending QC results to JSON of sample {wildcards.sample}"
    log:
        log="logs/{sample}_parseJson_afterQC.log"
    script:
        "scripts/parse_json.py"

rule filter_json:
    input:
        json_in="json/post_qc/{sample}.aquamis.json"
    params:
        sample="{sample}",
        config=config
    output:
        json_out="json/filtered/{sample}.aquamis.json"
    group: rule_directives.get("filter_json", {}).get("group", None)
    priority: rule_directives.get("filter_json", {}).get("priority", None)
    resources:
        cpus=1
    message:
        "Filtering results according to JSON schema for sample {wildcards.sample}"
    log:
        log="logs/{sample}_filterJson.log"
    script:
        "scripts/filter_json.py"

# %% deletion of ephemeral data -------------------------------------

rule delete_ephemeral_sample:
    input:
        json="json/post_assembly/{sample}.aquamis.json",
        assembly="Assembly/assembly/{sample}.fasta"
    params:
        sample="{sample}",
        ephemeral=config["smk_params"]["ephemeral"]
    output:
        log="logs/{sample}_ephemeral_data.log"
    group: rule_directives.get("delete_ephemeral_sample", {}).get("group", None)
    priority: rule_directives.get("delete_ephemeral_sample", {}).get("priority", None)
    resources:
        cpus=1
    message:
        "Ephemeral Mode - removing files of sample {wildcards.sample}"
    shell:
        """
        echo -e "[$(date +%Y-%m-%d" "%H:%M)]\tThe AQUAMIS pipeline has been executed with the option '--ephemeral'" > {output.log}
        # [[ {params.ephemeral} == "True" ]] && [[ -f "trimmed/{params.sample}_R1.fastq.gz"          ]] && rm --verbose -f  "trimmed/{params.sample}"* >> {output.log}  # TODO: comment-in for non-BeONE projects
        [[ {params.ephemeral} == "True" ]] && [[ -d "confindr/{params.sample}"                     ]] && rm --verbose -fR "confindr/{params.sample}" >> {output.log}
        [[ {params.ephemeral} == "True" ]] && [[ -d "Assembly/{params.sample}"                     ]] && rm --verbose -fR "Assembly/{params.sample}" >> {output.log}
        [[ {params.ephemeral} == "True" ]] && [[ -f "kraken2/{params.sample}.fromcontigs.taxonkit" ]] && rm --verbose -f  "kraken2/{params.sample}"* >> {output.log}
        """

rule delete_ephemeral_dirs:
    input:
        logs=expand("logs/{sample}_ephemeral_data.log", sample=samples.index)
    params:
        ephemeral=config["smk_params"]["ephemeral"]
    output:
        receipt="reports/data_availability.log"
    group: rule_directives.get("delete_ephemeral_dirs", {}).get("group", None)
    priority: rule_directives.get("delete_ephemeral_dirs", {}).get("priority", None)
    resources:
        cpus=1
    message:
        "Ephemeral Mode - removing directories of all samples"
    log:
        "logs/ephemeral_directories.log"
    shell:
        """
        echo -e "[$(date +%Y-%m-%d" "%H:%M)]\tThe AQUAMIS pipeline has been executed with the option '--ephemeral'" > {output.receipt}
        # [[ {params.ephemeral} == "True" ]] && [[ -d "trimmed"  ]] && rm --verbose -fR trimmed/  >> {output.receipt}  # TODO: comment-in for non-BeONE projects
        [[ {params.ephemeral} == "True" ]] && [[ -d "confindr" ]] && rm --verbose -fR confindr/ >> {output.receipt}
        [[ {params.ephemeral} == "True" ]] && [[ -d "kraken2"  ]] && rm --verbose -fR kraken2/  >> {output.receipt}
        """

# %% logging information --------------------------------------------

onstart:
    shell('echo -e "running\t`date +%Y-%m-%d" "%H:%M`" > pipeline_status_aquamis.txt')

onsuccess:
    shell('echo -e "success\t`date +%Y-%m-%d" "%H:%M`" > pipeline_status_aquamis.txt')

onerror:
    shell('echo -e "error\t`date +%Y-%m-%d" "%H:%M`" > pipeline_status_aquamis.txt'),
    print("An error occurred")
