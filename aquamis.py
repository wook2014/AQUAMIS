#!/usr/bin/env python3

import argparse
import subprocess
import os
import sys
import pandas as pd
from yaml import dump as yaml_dump

version = "1.3.8"


def get_gitversion(fixed_version: str):
    cwd = os.getcwd()
    os.chdir(os.path.dirname(os.path.realpath(__file__)))
    try:
        git_hash = subprocess.check_output(["git", "describe", "--always"]).decode('ascii').strip()
    except FileNotFoundError:
        git_hash = fixed_version
    os.chdir(cwd)  # NOTE: logdir argument removed
    return git_hash


def create_config(configfile, args):
    print("You are using aquamis version " + git_version)

    # checks
    if not os.path.exists(args.kraken2db):
        print("error: path to kraken2 database " + args.kraken2db + " does not exist")
        sys.exit(1)

    if not os.path.exists(args.taxonkit_db):
        print("error: path to taxonkit_db database " + args.taxonkit_db + " does not exist")
        sys.exit(1)

    if not os.path.exists(args.mashdb):
        print("error: path to mash database" + args.mashdb + " does not exist")
        sys.exit(1)

    if args.taxlevel_qc != "S" and args.taxlevel_qc != "G":
        print("error: Please specify flag --taxlevel_qc as either S or G. Your choice: " + args.taxlevel_qc + " is invalid")
        sys.exit(1)
    else:
        print("Your taxlevel_qc choice: " + args.taxlevel_qc + " is valid")

    ## adapt execution arguments to single-end input
    if args.single_end or args.iontorrent:
        aquamis_mode = "single-end"
        if args.snakefile == os.path.join(os.path.dirname(os.path.realpath(__file__)), "Snakefile"):
            args.snakefile = os.path.join(os.path.dirname(os.path.realpath(__file__)), "Snakefile_SingleEnd.smk")
        args.shovill_modules = args.shovill_modules.replace("--noreadcorr", "")  # remove incompatible option
    elif args.shovill_modules == "":
        aquamis_mode = "paired-end"
        args.shovill_modules = "--noreadcorr"  # add as default for paired-end

    if args.iontorrent:
        args.shovill_extraopts = (args.shovill_extraopts + " --iontorrent").strip() if not 'iontorrent' in args.shovill_extraopts else args.shovill_extraopts

    config_dict = {
        'Created by AQUAMIS': version,
        'version'           : git_version,
        'workdir'           : args.working_directory,
        'samples'           : args.sample_list,
        'smk_params'        : {
            'mode'           : aquamis_mode,
            'profile'        : args.profile,
            'rule_directives': args.rule_directives,
            'threads'        : args.threads_sample,
            'remove_temp'    : args.remove_temp,
            'ephemeral'      : args.ephemeral,
            'docker'         : args.docker
        },
        'params'            : {
            'run_name'   : args.run_name,
            'fastp'      : {'length_required': args.min_trimmed_length},
            'confindr'   : {'database': args.confindr_database},
            'kraken2'    : {'db_kraken'         : args.kraken2db,
                            'read_length'       : args.read_length,
                            'taxonomic_qc_level': args.taxlevel_qc,
                            'taxonkit_db'       : args.taxonkit_db},
            'shovill'    : {'depth'         : args.shovill_depth,
                            'output_options': args.shovill_output_options,
                            'tmpdir'        : args.shovill_tmpdir,
                            'thread_factor' : args.shovill_thread_factor,
                            'ram'           : args.shovill_ram,
                            'assembler'     : args.assembler,
                            'kmers'         : args.shovill_kmers,
                            'extraopts'     : args.shovill_extraopts,
                            'modules'       : args.shovill_modules
                            },
            'mash'       : {'mash_refdb'     : args.mashdb,
                            'mash_kmersize'  : args.mash_kmersize,
                            'mash_sketchsize': args.mash_sketchsize,
                            'mash_protocol'  : args.mash_protocol},
            'mlst'       : {'scheme': args.mlst_scheme},
            'quast'      : {'min_contig'  : args.quast_min_contig,
                            'min_identity': args.quast_min_identity},
            'qc'         : {'thresholds': args.qc_thresholds},
            'json_schema': {'validation': args.json_schema,
                            'filter'    : args.json_filter}
        }
    }

    with open(configfile, 'w') as outfile:
        yaml_dump(config_dict, outfile, default_flow_style=False, sort_keys=False)


def run_snakemake(configfile, args, sample_list):
    if args.forceall:
        force = "--forceall"
    elif not args.force:
        force = ""
    else:
        force = "--force {}".format(args.force)

    if args.no_assembly:
        force = "--force all_trimming_only"

    if args.ephemeral:
        force = "--force all_ephemeral"

    if args.unlock:
        unlock = "--unlock"
    else:
        unlock = ""

    if args.dryrun:
        dryrun = "--dryrun --quiet"
    else:
        dryrun = ""

    if args.remove_temp:
        remove_temp = ""
    else:
        remove_temp = "--notemp"

    if args.threads > 0:
        threads = "--cores {}".format(args.threads)
    elif args.profile == "none":
        threads = "--cores"  # use all available cores, if no profile AND no cores are specified
    else:
        threads = ""  # use cores according to Snakemake profile config.yaml

    if args.conda_frontend:
        frontend = "conda"
    else:
        frontend = "mamba"

    call = "snakemake" \
           " --profile {smk_profile}" \
           " --configfile {configfile}" \
           " --config samples={sample_list}" \
           " --snakefile {snakefile}" \
           " {threads}" \
           " {force}" \
           " {remove_temp}" \
           " {dryrun}" \
           " {unlock}".format(
        smk_profile=smk_profile,
        configfile=configfile,
        sample_list=sample_list,
        snakefile=args.snakefile,
        threads=threads,
        force=force,
        remove_temp=remove_temp,
        dryrun=dryrun,
        unlock=unlock)

    if args.use_conda:
        call = call + " --use-conda" \
                      " --conda-prefix {conda_prefix}" \
                      " --conda-frontend {conda_frontend}".format(
            conda_prefix=args.condaprefix,
            conda_frontend=frontend)

    print(call)
    subprocess.call(call, shell=True)


def find_missing(args):
    samples = pd.read_csv(args.sample_list, index_col="sample", sep="\t")
    samples.index = samples.index.astype('str', copy=False)  # in case samples are integers, need to convert them to str

    # find present samples
    present_set = set()
    for s in samples.index:
        if os.path.isfile(os.path.join(args.working_directory, "json", "post_assembly", s + ".json")):
            present_set.add(s)

    print("Found the following samples: ", present_set)

    # subset pandas dataframe with present samples

    # if len(present_set) == 1:
    #     samples_present = samples.loc[[present_set],]  # TODO: causes TypeError: unhashable type: 'set', 20211217 HB
    # else:
    samples_present = samples.loc[present_set,]

    # write corrected sample sheet
    sample_list_corrected = os.path.join(args.working_directory, "samples_fixed.tsv")
    samples_present.to_csv(sample_list_corrected, sep="\t", index=True)

    # write fails to file
    failsfile = open(os.path.join(args.working_directory, "Assembly", "fails.txt"), "w")
    fail_counter = 0
    for sample in samples.index:
        if sample not in present_set:
            fail_counter += 1
            failsfile.write(sample + "\n")
    print("Number of fails: ", fail_counter)

    failsfile.close()
    return sample_list_corrected


def main():
    parser = argparse.ArgumentParser()

    # path arguments
    parser.add_argument('--sample_list', '-l', help='List of samples to assemble, format as defined by ...',
                        default="/", type=os.path.abspath, required=False)
    parser.add_argument('--working_directory', '-d', help='Working directory',
                        default="/", type=os.path.abspath, required=False)
    parser.add_argument('--snakefile', '-s', help='Path to Snakefile of bakcharak pipeline; default: path to Snakefile in same directory',
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "Snakefile"), type=os.path.abspath, required=False),
    parser.add_argument('--run_name', '-r', help='Name of the sequencing run for all samples in the sample list, e.g. "210401_M02387_0709_000000000-HBXX6", or a self-chosen analysis title',
                        default="", required=False)
    # docker arguments
    parser.add_argument('--docker', help='Mapped volume path of the host system',
                        default="", required=False)
    # qc tresholds arguments
    parser.add_argument('--qc_thresholds', help='Definition of thresholds in JSON file; default: ' + os.path.join(os.path.dirname(os.path.realpath(__file__)), "thresholds", "AQUAMIS_thresholds.json"),
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "resources", "AQUAMIS_thresholds.json"), type=os.path.abspath, required=False)
    # json schema arguments
    parser.add_argument('--json_schema', help='JSON schema used for validation; default: ' + os.path.join(os.path.dirname(os.path.realpath(__file__)), "resources", "AQUAMIS_schema_v20210226.json"),
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "resources", "AQUAMIS_schema_v20220125.json"), type=os.path.abspath, required=False)
    parser.add_argument('--json_filter', help='Definition of thresholds in JSON file; default: ' + os.path.join(os.path.dirname(os.path.realpath(__file__)), "thresholds", "AQUAMIS_schema_filter_v20210226.json"),
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "resources", "AQUAMIS_schema_filter_v20220125.json"), type=os.path.abspath, required=False)
    # fastp arguments
    parser.add_argument('--min_trimmed_length', help='Minimum length of a read to keep; default: 15',
                        default=15, type=int, required=False)
    # mash arguments
    parser.add_argument('--mashdb', help='Path to reference mash database; default:' + os.path.join(os.path.dirname(os.path.realpath(__file__)), "reference_db", "mash", "mashDB.msh"),
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "reference_db", "mash", "mashDB.msh"), type=os.path.abspath, required=False)
    parser.add_argument('--mash_kmersize', help='kmer size for mash, must match size of database; default: 21',
                        default=21, type=int, required=False)
    parser.add_argument('--mash_sketchsize', help='sketch size for mash, must match size of database; default: 1000',
                        default=1000, type=int, required=False)
    parser.add_argument('--mash_protocol', help='Transfer protocol for reference retrieval, choose between https or ftp; default: "https"',
                        default="https", required=False)
    # kraken arguments
    parser.add_argument('--kraken2db', help='Path to kraken2 database; default: ' + os.path.join(os.path.dirname(os.path.realpath(__file__)), "reference_db", "kraken2"),
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "reference_db", "kraken"), type=os.path.abspath, required=False)  # Note: minikraken2.tar.gz extracts to "../kraken/" not kraken2
    parser.add_argument('--taxlevel_qc', help='Taxonomic level for kraken2 classification quality control. Choose S for species or G for genus; default: "G"',
                        default="G", required=False)
    parser.add_argument('--read_length', help='Read length to be used in bracken abundance estimation; default: 150',
                        default=150, type=int, required=False)
    parser.add_argument('--taxonkit_db', help='Path to taxonkit_db; default:' + os.path.join(os.path.dirname(os.path.realpath(__file__)), "reference_db", "taxonkit"),
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "reference_db", "taxonkit"), type=os.path.abspath, required=False)
    # confindr database
    parser.add_argument('--confindr_database', help='Path to confindr databases; default:' + os.path.join(os.path.dirname(os.path.realpath(__file__)), "reference_db", "confindr"),
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "reference_db", "confindr"), type=os.path.abspath, required=False)
    # assembly options
    parser.add_argument('--shovill_tmpdir', help='Fast temporary directory; default: "/tmp/shovill"',
                        default="/tmp/shovill", type=os.path.abspath, required=False)
    parser.add_argument('--shovill_thread_factor', help='Factor to increase threads_sample for shovill ; default: 3',
                        default=3, type=int, required=False)
    parser.add_argument('--shovill_ram', help='Limit amount of RAM (in GB, integer) provided to shovill; default: 16',
                        default=16, type=int, required=False)
    parser.add_argument('--shovill_depth', help='Sub-sample --R1/--R2 to this depth. Disable with --depth 0; default: 100',
                        default=100, type=int, required=False)
    parser.add_argument('--assembler', help='Assembler to use in shovill, choose from megahit velvet skesa spades; default: "spades"',
                        default="spades", required=False)
    parser.add_argument('--shovill_kmers', help='K-mers to use <blank=AUTO>; default: ""',
                        default="", required=False)
    parser.add_argument('--shovill_extraopts', help='Extra assembler options in quotes and equal sign notation e.g. spades: --shovill_extraopts="--iontorrent"; default: ""',
                        default="", required=False)
    parser.add_argument('--shovill_modules', help='Module options for shovill; choose from --noreadcorr --trim --nostitch --nocorr; default: "--noreadcorr"; Note: add choices as string in quotation marks',
                        default="", required=False)
    parser.add_argument('--shovill_output_options', help='Extra options for shovill; default: ""',
                        default="", required=False)
    # mlst options
    parser.add_argument('--mlst_scheme', help='Extra option for MLST; default: ""',
                        default="", required=False)
    # quast options
    parser.add_argument('--quast_min_contig', help='Extra option for QUAST; default: 500',
                        default=500, type=int, required=False)
    parser.add_argument('--quast_min_identity', help='Extra option for QUAST; default: 80',
                        default=80, type=float, required=False)
    # cpu arguments
    parser.add_argument('--use_conda', help='Utilize the Snakemake "--useconda" option, i.e. Smk rules require execution with a specific conda env',
                        default=False, action='store_true', required=False)
    parser.add_argument('--conda_frontend', help='Do not use mamba but conda as frontend to create individual conda environments',
                        default=False, action='store_true', required=False)
    parser.add_argument('--condaprefix', '-c', help='Path of default conda environment, enables recycling built environments; default: "<AQUAMIS>/conda_env"',
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "conda_env"), required=False)
    parser.add_argument('--threads', '-t', help='Number of Threads/Cores to use. This overrides the "<AQUAMIS>/profiles" settings',
                        default=0, type=int, required=False)
    parser.add_argument('--threads_sample', '-p', help='Number of Threads to use per sample in multi-threaded rules; default: 1',
                        default=1, type=int, required=False)
    parser.add_argument('--force', '-f', help='Snakemake force. Force recalculation of output (rule or file) speciefied here',
                        default=False, type=str, required=False)
    parser.add_argument('--forceall', help='Snakemake force. Force recalculation of all steps',
                        default=False, action='store_true', required=False)
    parser.add_argument('--no_assembly', help='Snakemake All-Rule: Only trimming and kraken analysis',
                        default=False, action='store_true', required=False)
    parser.add_argument('--single_end', help='Use only single-end reads as input',
                        default=False, action='store_true', required=False)
    parser.add_argument('--iontorrent', help='Use single-end Ion Torrent reads as input',
                        default=False, action='store_true', required=False)
    parser.add_argument('--ephemeral', help='Snakemake All-Rule: Remove all temporary data except result JSONs and Reports',
                        default=False, action='store_true', required=False)
    parser.add_argument('--remove_temp', help='Remove large temporary files. May lead to slower re-runs but saves disk space',
                        default=False, action = 'store_true', required=False)
    parser.add_argument('--rule_directives', help='Process DAG with rule grouping and/or rule prioritization via Snakemake rule directives YAML; default: "<AQUAMIS>/profiles/smk_directives.yaml" equals no directives',
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), "profiles", "smk_directives.yaml"), type=os.path.abspath, required=False)
    parser.add_argument('--profile', help='(A) Full path or (B) directory name under "<AQUAMIS>/profiles" of a Snakemake config.yaml with Snakemake parameters, e.g. available CPUs and RAM. Default: "workstation"',
                        default="none", type=str, required=False)
    parser.add_argument('--fix_fails', help='Re-run Snakemake after failure removing failed samples',
                        default=False, action='store_true', required=False)
    parser.add_argument('--dryrun', '-n', help='Snakemake dryrun. Only calculate graph without executing anything',
                        default=False, action='store_true', required=False)
    parser.add_argument('--unlock', help='Unlock a Snakemake execution folder if it had been interrupted',
                        default=False, action='store_true', required=False)
    parser.add_argument('--version', '-V', help='Print program version',
                        default=False, action='store_true', required=False)

    args = parser.parse_args()

    # git version
    global git_version
    git_version = get_gitversion(fixed_version=version)

    if args.version:
        print("AQUAMIS version ", version)
        if not version == git_version:
            print("Commit version ", git_version)
        return

    # checks
    if args.sample_list == "/":
        print("error: the following argument is required: -l/--sample_list")
        sys.exit(1)

    if not os.path.exists(args.sample_list):
        print("error: path to sample list " + args.sample_list + " does not exist")
        sys.exit(1)

    if args.working_directory == "/":
        print("error: the following argument is required: -d/--working_directory")
        sys.exit(1)

    if not os.path.exists(args.working_directory):
        os.makedirs(args.working_directory)

    global smk_profile
    if os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)), "profiles", args.profile)):
        smk_profile = os.path.join(os.path.dirname(os.path.realpath(__file__)), "profiles", args.profile)
    elif os.path.exists(os.path.realpath(args.profile)):
        smk_profile = os.path.realpath(args.profile)
    elif os.path.exists(os.path.join(os.path.dirname(os.path.realpath(__file__)), "profiles", "workstation")):
        smk_profile = os.path.join(os.path.dirname(os.path.realpath(__file__)), "profiles", "workstation")
    else:
        print("error: path to Snakemake config.yaml " + args.profile + " does not exist")
        sys.exit(1)

    configfile = os.path.join(args.working_directory, "config_aquamis.yaml")
    create_config(configfile, args)

    run_snakemake(configfile, args, sample_list=args.sample_list)

    if args.fix_fails and not args.dryrun and not args.unlock:
        if not os.path.isfile(os.path.join(args.working_directory, "reports", "assembly_report.html")):
            print("Re-evaluating samples")
            sample_list_corrected = find_missing(args)
            run_snakemake(configfile, args, sample_list=sample_list_corrected)


if __name__ == '__main__':
    main()
