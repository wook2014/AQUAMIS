#!/bin/bash

## HOWTO: execute any sudo command first, e.g. 'sudo ls', before running this script or check presence of file $HOME/.sudo_as_admin_successful
## Note: this script has to reside in AQUAMIS/scripts

# Retrieve script paths and derive source database directory
SCRIPT=$(readlink -f $0)
SCRIPT_PATH=$(dirname $SCRIPT)
INSTALL_PATH=$(dirname $SCRIPT_PATH)
KRAKEN_PATH=$(realpath ${INSTALL_PATH}/reference_db/kraken)

# Create RAMdisk and copy Kraken2 database
[[   -d /mnt/ramdisk ]] && echo "RAMdisk path already exists, skipping mount..."
[[ ! -d /mnt/ramdisk ]] && sudo mount -t tmpfs -o size=8G tmpfs /mnt/ramdisk
[[   -d /mnt/ramdisk/kraken ]] && echo "Kraken2 directory already exists on RAMdisk, skipping mkdir..."
[[ ! -d /mnt/ramdisk/kraken ]] && sudo mkdir /mnt/ramdisk/kraken && sudo chown $USER /mnt/ramdisk/kraken
[[ ! -f $KRAKEN_PATH/hash.k2d ]] && echo "Kraken2 path '$KRAKEN_PATH' does not contain a hash.k2d file." && exit 0
[[   -f $KRAKEN_PATH/hash.k2d ]] && rsync -ruP $KRAKEN_PATH/* /mnt/ramdisk/kraken/
