#!/bin/bash
set -euo pipefail

# Pull the latest version of the image, in order to populate the build cache:
#docker pull bfrbioinformatics/aquamis:databases || true

# Build the databases:
echo "Building bfrbioinformatics/aquamis:databases"
docker build --target databases \
       --cache-from=bfrbioinformatics/aquamis:databases \
       --build-arg BUILD_DATE=$(date +"%Y%m%d%H%M%S") \
       --tag bfrbioinformatics/aquamis:databases .

# Push the new versions:
echo "Pushing bfrbioinformatics/aquamis:databases"
docker push bfrbioinformatics/aquamis:databases
