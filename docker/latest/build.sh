#!/bin/bash
set -euo pipefail

# Pull the latest version of the image, in order to populate the build cache:
docker pull bfrbioinformatics/aquamis:databases || true
docker pull bfrbioinformatics/aquamis:testdata  || true
docker pull bfrbioinformatics/aquamis:conda     || true
#docker pull bfrbioinformatics/aquamis:latest    || true

# Build the application stage, using cached databases & testdata stage:
echo "Building bfrbioinformatics/aquamis:latest"
docker build --target latest \
       --cache-from=bfrbioinformatics/aquamis:databases \
       --cache-from=bfrbioinformatics/aquamis:testdata \
       --cache-from=bfrbioinformatics/aquamis:conda \
       --cache-from=bfrbioinformatics/aquamis:latest \
       --build-arg BUILD_DATE=$(date +"%Y%m%d%H%M%S") \
       --tag bfrbioinformatics/aquamis:full \
       --tag bfrbioinformatics/aquamis:latest .

# Push the new versions:
echo "Pushing bfrbioinformatics/aquamis:latest"
docker push bfrbioinformatics/aquamis:full
docker push bfrbioinformatics/aquamis:latest