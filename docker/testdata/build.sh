#!/bin/bash
set -euo pipefail

# Pull the latest version of the image, in order to populate the build cache:
#docker pull bfrbioinformatics/aquamis:testdata  || true

# Build the test data:
echo "Building bfrbioinformatics/aquamis:testdata"
docker build --target testdata \
       --cache-from=bfrbioinformatics/aquamis:testdata \
       --build-arg BUILD_DATE=$(date +"%Y%m%d%H%M%S") \
       --tag bfrbioinformatics/aquamis:testdata .

# Push the new versions:
echo "Pushing bfrbioinformatics/aquamis:testdata"
docker push bfrbioinformatics/aquamis:testdata