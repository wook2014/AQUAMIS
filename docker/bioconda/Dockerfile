##### STAGE - Build Application ###############################################
ARG BIOCONDA_CONTAINER
FROM $BIOCONDA_CONTAINER AS bioconda

ARG BIOCONDA_VERSION
ARG BUILD_DATE
LABEL copyright="Carlus Deneke, Holger Brendebach, Simon Tausch"
LABEL docker-maintainer="Holger Brendebach <holger.brendebach (at) bfr.bund.de>"
LABEL company="German Federal Institute for Risk Assessment (BfR)"
LABEL description="Assembly-based QUality Assessment for Microbial Isolate Sequencing"
LABEL git="https://gitlab.com/bfr_bioinformatics/AQUAMIS"
LABEL bioconda-version=$BIOCONDA_VERSION
LABEL build-date=$BUILD_DATE

# Augment AQUAMIS via Bioconda
SHELL ["/bin/bash", "-c"]
RUN mkdir -p /AQUAMIS/analysis

# Copy Data from Earlier Stages
COPY --from=bfrbioinformatics/aquamis:databases --chown=root:root /AQUAMIS/reference_db               /usr/local/opt/aquamis/reference_db
COPY --from=bfrbioinformatics/aquamis:databases --chown=root:root /AQUAMIS/reference_db/bacteria      /usr/local/lib/python3.7/site-packages/quast_libs/busco/bacteria/
COPY --from=bfrbioinformatics/aquamis:databases --chown=root:root /AQUAMIS/reference_db/augustus3.2.3 /usr/local/lib/python3.7/site-packages/quast_libs/augustus3.2.3/
COPY --from=bfrbioinformatics/aquamis:testdata  --chown=root:root /AQUAMIS/test_data                  /AQUAMIS/test_data

# Hotfix of missing timezone database in Bioconda Busybox until tzdata is added to Conda recipe TODO: remove after fix
ADD --chown=root:root zoneinfo /usr/share/zoneinfo

# Create Sample Sheet for Test Data
RUN /usr/local/opt/aquamis/scripts/create_sampleSheet.sh --mode ncbi --fastxDir /AQUAMIS/test_data/fastq --outDir /AQUAMIS/test_data

# Set Docker Workdir and Entrypoint when container is started
WORKDIR /AQUAMIS/analysis
